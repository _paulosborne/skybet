import React from 'react';
import ampersandMixin from 'ampersand-react-mixin';
import classnames from 'classnames';
import ReceiptTable from './bet-slip/receipt-table';
import BetSlipTable from './bet-slip/bet-slip-table';

export default React.createClass({
    mixins: [ampersandMixin],
    displayName: 'betSlips',
    onTabClick (event) {
        const {status, tab} = event.currentTarget.dataset;
        this.setState({ showStatus: status, tabActive: tab });
    },
    _minimize () {
        this.setState({ minimize: !this.state.minimize });
    },
    getInitialState () {
        return {
            minimize: true,
            showStatus: 'pending',
            tabActive: '1'
        };
    },
    render () {
        const {slips} = this.props;
        const counter = slips.countBy('status');
        const classes = classnames('c-betslip', {
            minimize: slips.length ? false : true
        });
        let content;

        if (this.state.showStatus === 'complete') {
            content = (<ReceiptTable {...this.props}/>);
        } else if (slips.where({ status: 'pending'}).length) {
            content = (<BetSlipTable {...this.props}/>);
        } else {
            content = (
                <div className="no-pending-bets">
                    Your bet slip is empty
                </div>
            );
        }

        return (
            <div className={classes}>
                <div className="container">
                <ul role="navigation" >
                    <li className={this.state.tabActive === '1' ? 'is-active' : ''} onClick={this.onTabClick} data-tab="1" data-status="pending">My Bets Slip ({counter.pending || 0})</li>
                    <li className={this.state.tabActive === '2' ? 'is-active' : ''} onClick={this.onTabClick} data-tab="2" data-status="complete">Receipts ({counter.complete || 0})</li>
                </ul>
                    {content}
                </div>
            </div>
        );
    }
});
