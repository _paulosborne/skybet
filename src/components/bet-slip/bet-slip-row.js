import React from 'react';
import ampersandMixin from 'ampersand-react-mixin';
import money from 'accounting';
import validator from 'validator';

export default React.createClass({
    displayName: 'BetSlipRow',
    mixins: [ampersandMixin],
    getInitialState () {
        return {
            stake_valid: true
        };
    },
    onRemoveBet () {
        const {slip} = this.props;
        slip.collection.remove(slip);
    },
    onChangeStake (event) {
        const value = event.target.value;
        const {slip} = this.props;
        const opts = {
            allow_negatives: false
        };

        if (!validator.isCurrency(value, opts)) {
            this.setState({ stake_valid: false });
            return;
        } else {
            this.setState({ stake_valid: true});
        }

        slip.set('stake', value);
    },
    componentDidMount () {
        React.findDOMNode(this.refs.stakeInput).focus();
    },
    render () {
        const {slip} = this.props;
        return (
            <tr>
                <td><span className="remove-bet" onClick={this.onRemoveBet}></span></td>
                <td>{slip.bet.event}</td>
                <td>{slip.bet.name}</td>
                <td>
                    <input
                        ref="stakeInput"
                        className={!this.state.stake_valid ? 'error' : ''}
                        onChange={this.onChangeStake}
                        type='number' defaultValue="0.00"
                    /></td>
                <td>{slip.bet.displayOdds()}</td>
                <td>&pound;{money.formatNumber(slip.stakeReturn, 2)}</td>
            </tr>
        );
    }
});
