import React from 'react';
import ampersandMixin from 'ampersand-react-mixin';
import money from 'accounting';

export default React.createClass({
    displayName: 'ReceiptRow',
    mixins: [ampersandMixin],
    render () {
        const {slip} = this.props;
        return (
            <tr>
                <td>{slip.transaction_id}</td>
                <td>{slip.bet.event}</td>
                <td>{slip.bet.name}</td>
                <td>&pound;{money.formatNumber(slip.stake, 2)}</td>
                <td>{slip.bet.displayOdds()}</td>
                <td>&pound;{money.formatNumber(slip.stakeReturn, 2)}</td>
            </tr>
        );
    }
});
