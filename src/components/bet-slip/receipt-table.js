import React from 'react';
import ampersandMixin from 'ampersand-react-mixin';
import ReceiptRow from './receipt-row';

export default React.createClass({
    displayName: 'ReceiptTable',
    mixins: [ampersandMixin],
    render () {
        const {slips} = this.props;
        return (
            <table>
                <tr>
                    <th>Transaction ID</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>Stake</th>
                    <th>Odds</th>
                    <th>Returns</th>
                </tr>
                <tbody>
                    {slips.where({ status: 'complete' }).map((slip) => {
                        return (<ReceiptRow key={slip.cid} slip={slip}/>);
                    })}

                </tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><strong>Total Return:</strong></td>
                    <td>&pound;{slips.potentialWinnings()}</td>
                </tr>
            </table>

        );
    }
});
