import React from 'react';
import ampersandMixin from 'ampersand-react-mixin';
import BetSlipRow from './bet-slip-row';
import pluralize from 'pluralize';

export default React.createClass({
    displayName: 'BetSlipTable',
    mixins: [ampersandMixin],
    onPlaceBets () {
        this.props.slips.processPendingBets();
    },
    render () {
        const {slips} = this.props;
        const label = pluralize('Place Bet', slips.length);
        return (
            <div>
                <table className="betlist">
                    <tr>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>Stake</th>
                        <th>Odds</th>
                        <th width="20%">Returns</th>
                    </tr>
                    <tbody>
                        {slips.where({ status: 'pending' }).map((slip) => {
                            return (
                                <BetSlipRow key={slip.cid} slip={slip}/>
                            );
                        })}
                    </tbody>
                </table>
                <button className="place-bets primary" onClick={this.onPlaceBets}>{label}</button>
            </div>
        );
    }
});
