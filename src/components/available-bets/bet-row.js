import React from 'react';
import ampersandMixin from 'ampersand-react-mixin';

export default React.createClass({
    mixins: [ampersandMixin],
    displayName: 'BetRow',
    onAddBet () {
        const {bet, slips} = this.props;
        const slip = slips.get(bet.bet_id);

        if (slip && slip.transaction_id) {
            return;
        }

        if (slips.get(bet.bet_id)) {
            slips.remove(bet.bet_id);
        } else {
            slips.add({ bet_id: bet.bet_id});
        }
    },
    render () {
        const {bet, slips} = this.props;
        const slip = slips.get(bet.bet_id);

        return (
            <tr className="selected">
                <td>{bet.event}</td>
                <td>{bet.name}</td>
                <td className={slip ? slip.status : ''} onClick={this.onAddBet}>{slip ? '' : bet.displayOdds()}</td>
            </tr>
        );
    }
});
