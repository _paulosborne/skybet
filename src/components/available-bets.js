import React from 'react';
import ampersandMixin from 'ampersand-react-mixin';
import BetRow from './available-bets/bet-row';

export default React.createClass({
    mixins: [ampersandMixin],
    render () {
        return (
            <div className="c-available-bets twelve">
                <table>
                    <tbody>
                        {this.props.bets.map((bet) => {
                            return (<BetRow key={bet.bet_id} bet={bet} slips={this.props.slips}/>);
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
});
