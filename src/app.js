import Router from './router';
import app from 'ampersand-app';
import BetCollection from './models/bet-collection';
import BetSlipCollection from './models/bet-slip-collection';

app.extend({
    init () {
        this.bets = new BetCollection();
        this.slips = new BetSlipCollection([], {
            parent: this.bets
        });
        this.bets.fetch();
        this.router = new Router();
        this.router.history.start();
    }
});

app.init();
