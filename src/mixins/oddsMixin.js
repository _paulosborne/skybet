import has from 'lodash.has';

export default {
    displayOdds (numerator, denominator) {
        if (arguments.length === 0 && has(this.odds, 'numerator')) {
            ({numerator, denominator} = this.odds);
        }

        if (numerator === denominator) {
            return 'evens';
        }
        return numerator + '/' + denominator;
    }
};
