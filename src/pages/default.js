import React from 'react';
import AvailableBets from '../components/available-bets';
import BetSlip from '../components/bet-slips';

export default React.createClass({
    render () {
        return (
            <div>
                <img class="logo" src="//st1.skybet.com/static/bet/img/sky-bet.png" alt="Sky Bet"/>
                <AvailableBets {...this.props}/>
                <BetSlip {...this.props}/>
            </div>
        );
    }
});
