import Router from 'ampersand-router';
import React from 'react';
import app from 'ampersand-app';
import Styles from './styles/main.scss';
import Layout from './layout';
import Default from './pages/default';

export default Router.extend({
    routes: {
        '': 'home'
    },
    renderPage (page, opts = { layout: true }) {
        let output = page;
        if (opts.layout) {
            output = (
                <Layout>
                    {page}
                </Layout>
            );
        }
        React.render(output, document.body);
    },
    home () {
        this.renderPage(<Default bets={app.bets} slips={app.slips}/>);
    }
});
