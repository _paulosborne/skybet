import Collection from 'ampersand-rest-collection';
import BetModel from './bet-model';

export default Collection.extend({
    url: 'http://skybettechtestapi.herokuapp.com/available',
    model: BetModel
});
