import Collection from 'ampersand-rest-collection';
import BetSlipModel from './bet-slip-model';
import money from 'accounting';

export default Collection.extend({
    model: BetSlipModel,
    mainIndex: 'bet_id',
    processPendingBets () {
        this.where({ status: 'pending' }).forEach((model) => {
            if (model.stake > 0) {
                model.placeBet();
            }
        });
    },
    potentialWinnings () {
        const winnings = this.reduce((total, model) => {
            if (!model.transaction_id) {
                return;
            }
            return total + model.stakeReturn;
        }, 0);

        return money.formatNumber(winnings, 2);
    }
});
