import Model from 'ampersand-model';
import float from 'float';
import xhr from 'xhr';

export default Model.extend({
    idAttribute: 'bet_id',
    props: {
        bet_id: 'number',
        status: {
            type: 'string',
            default: 'pending',
            values: ['pending', 'complete', 'failed']
        },
        transaction_id: 'number',
        stake: {
            default: 0,
            type: 'decimal'
        }
    },
    dataTypes: {
        'decimal': {
            set (newVal) {
                let value = newVal;

                if (newVal < 0) {
                    value = 0;
                }

                return {
                    val: float.round(value, 2),
                    type: 'decimal'
                };
            },
            compare (currentVal, newVal) {
                return currentVal === newVal;
            }
        }
    },
    derived: {
        bet: {
            deps: ['bet_id'],
            fn () {
                return this.collection.parent.findWhere({ bet_id: this.bet_id });
            }
        },
        stakeReturn: {
            deps: ['stake'],
            fn () {
                const {numerator: n, denominator: d} = this.bet.odds;
                const value = ((this.stake / d) * n) + this.stake;
                return float.round(value, 2);
            }
        }
    },
    placeBet () {
        const self = this;

        xhr({
            url: 'http://skybettechtestapi.herokuapp.com/bets',
            method: 'post',
            json: {
                bet_id: this.bet_id,
                odds: this.bet.odds,
                stake: this.stake
            }
        }, (err, response, body) => {
            let props = {};

            if (response.statusCode === 418) {
                props.status = 'failed';
            } else if (response.statusCode === 201) {
                props.status = 'complete';
                props.transaction_id = body.transaction_id;
            }

            self.set(props);
        });
    }
});
