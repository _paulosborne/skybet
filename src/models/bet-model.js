import Model from 'ampersand-model';
import oddsMixin from '../mixins/oddsMixin';

export default Model.extend(oddsMixin, {
    props: {
        bet_id: 'number',
        event: 'string',
        name: 'string',
        odds: 'object'
    }
});
