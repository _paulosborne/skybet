#skybet techtest

Please find instructions below on how to get this application running on your local machine (npm is required).

1. "npm install" installs required dependencies
2. "npm start" will start a local webserver which should run at http://localhost:3000
3. "npm test" will run the tests
