import test from 'tape'
import random from 'lodash.random'
import BetModel from '../src/models/bet-model'
import {generateBet} from './utils'

test('bet-model', function (t) {
    test('[bet-model] smoke test', function (t) {
        const obj = generateBet(1)
        const model = new BetModel(obj)

        t.plan(5)
        t.equal(model.bet_id, obj.bet_id ,'should set the bet id')
        t.equal(model.event, obj.event, 'should set the event name')
        t.equal(model.name, obj.name, 'should set the bet name')
        t.equal(model.odds.numerator, obj.odds.numerator, 'should set the numerator')
        t.equal(model.odds.denominator, obj.odds.denominator, 'should set the denominator')
        t.end()
    })
    t.end()
})
