import test from 'blue-tape'
import {generateBet} from './utils'
import BetCollection from '../src/models/bet-collection'
import BetSlipCollection from '../src/models/bet-slip-collection'
import BetModel from '../src/models/bet-model'

test('bet-slip-collection', (t) => {
    test('[bet-slip-collection] smoke test', (t) => {
        const bets = [1,2,3].map((n) => { return { bet_id: n }})
        const slipCollection = new BetSlipCollection(bets)
        t.plan(1)
        t.equal(slipCollection.length, 3, 'should accept an array of bets')
    })

    test('[bet-slip-collection] setting parent collection', (t) => {
        const list = [1,2,3,4,5].map((n) => { return generateBet(n) })
        const bc = new BetCollection(list)
        const bsc = new BetSlipCollection([{ bet_id: list[2].bet_id }], { parent: bc })
        const slip = bsc.findWhere({ bet_id: list[2].bet_id })

        t.equal(bsc.where({ bet_id: list[2].bet_id }).length, 1, 'should return an array containing one model')
        t.ok(slip.bet instanceof BetModel, 'should be an instance of BetModel');
        t.deepEqual(slip.bet.serialize(), list[2], 'should have the same values')
        t.ok(slip.collection.parent instanceof BetCollection, 'should have a parent BetCollection')
        t.end()
    })
    t.end()
})
