import random from 'lodash.random'
import uniqueId from 'lodash.uniqueid'

export function generateBet (id, betEvent, betName, numerator, denominator) {
    let bet = {}

    bet.bet_id = id
    bet.event = uniqueId('event-')
    bet.name = uniqueId('name-')

    bet.odds = {}
    bet.odds.numerator = numerator || random(1,10)
    bet.odds.denominator = denominator || random(1,10)

    return bet;
}
