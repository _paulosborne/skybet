import test from 'tape'
import random from 'lodash.random'
import {generateBet} from './utils'
import BetSlipModel from '../src/models/bet-slip-model'
import BetSlipCollection from '../src/models/bet-slip-collection'
import BetCollection from '../src/models/bet-collection'


test('bet-slip-model', (t) => {
    test('[bet-slip-model] smoke test', (t) => {
        const id = random(1,100)
        const slip = new BetSlipModel({
            bet_id: id,
        })
        t.plan(2)
        t.equal(slip.bet_id, id, 'should set the bet_id')
        t.equal(slip.stake, 0, 'should set the default stake to zero')
    })

    test('[bet-slip-model] setting stake', function (t) {
        const stake = 2.25
        const bets = [1,2,3,4,5].map((n) => { return generateBet(n) })
        const bc = new BetCollection(bets)
        const bsc = new BetSlipCollection([
            { bet_id: 1 },
            { bet_id: 2 }
        ],{
            parent: bc
        })

        let slip = bsc.at(0)
        slip.stake = stake


        t.end()
    })
    t.end()
})
