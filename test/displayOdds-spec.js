import test from 'tape'
import {displayOdds} from '../src/mixins/oddsMixin'

test('displayOdds', function (t) {
    t.plan(2);
    t.equal(displayOdds(10,1), '10/1', 'should return a formatted string "10/1"');
    t.equal(displayOdds(1,1), 'evens', 'should return evens if numerator equals denominator');
    t.end();
})
